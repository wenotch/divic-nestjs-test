## Description

[Nest](https://github.com/nestjs/nest) This was built using nestjs, graphql and prisma

## Prerequiste

- PostgresSQL
- Nodejs

## Installation

```bash
$ npm install
```

## Setting up prisma

```bash
$ npx prisma generate
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Documentation

while server is running, visit localhost:3000/graphql

## Developer

Emmanuel C. Nwanochie - Assessment for divic nestjs role
emmanuelnwanochie247@gmail.com

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
