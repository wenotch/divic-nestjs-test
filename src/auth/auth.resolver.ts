import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { Auth } from './entities/auth.entity';
import { SignUpInput } from './dto/signup-input';
import { SignResponse } from './dto/sign-reponse';
import { SignInInput } from './dto/signin-input';
import { BiometricInput } from './dto/biometric-signin';

@Resolver(() => Auth)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  /**
   * Sign up a new user
   * @param signUpInput - The data for the new user
   * @returns The response containing the access and refresh tokens and the user
   */
  @Mutation(() => SignResponse)
  signup(@Args('signUpInput') signUpInput: SignUpInput) {
    return this.authService.signup(signUpInput);
  }

  /**
   * Sign in an existing user
   * @param signInInput - The user's email and password
   * @returns The response containing the access and refresh tokens and the user
   */
  @Mutation(() => SignResponse)
  signin(@Args('signInInput') signInInput: SignInInput) {
    return this.authService.signin(signInInput);
  }

  /**
   * Sign in a user using biometric authentication
   * @param biometricInput - The user's biometric key
   * @returns The response containing the access and refresh tokens and the user
   */
  @Mutation(() => SignResponse)
  biometricSignin(@Args('biometricInput') biometricInput: BiometricInput) {
    return this.authService.biometricSignin(biometricInput);
  }

  /**
   * A simple hello world query
   * @returns "Hello, World!"
   */
  @Query(() => String)
  hello(): string {
    return 'Hello, World!';
  }
}
