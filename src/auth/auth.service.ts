import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { SignUpInput } from './dto/signup-input';
import { PrismaService } from './../prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import * as argon from 'argon2';
import { SignInInput } from './dto/signin-input';
import { BiometricInput } from './dto/biometric-signin';
@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  /**
   * Sign up a new user with the provided credentials.
   * @param {SignUpInput} signUpInput - The user's sign up input.
   * @returns {Promise<{accessToken: string, refreshToken: string, user: User}>} - The user's access token, refresh token, and user object.
   * @throws {BadRequestException} If the user's email or biometric key already exists.
   */
  async signup(signUpInput: SignUpInput) {
    const hashedPassword = await argon.hash(signUpInput.password);

    const userExists = await this.prisma.user.findUnique({
      where: { email: signUpInput.email },
    });
    if (userExists) throw new BadRequestException('Account Already exist');

    const keyExist = await this.prisma.user.findUnique({
      where: { biometricKey: signUpInput.biometricKey },
    });
    if (keyExist) throw new BadRequestException('Account Already exist');

    const user = await this.prisma.user.create({
      data: {
        biometricKey: signUpInput.biometricKey,
        hashedPassword,
        email: signUpInput.email,
      },
    });

    const { accessToken, refreshToken } = await this.createTokens(
      user.id,
      user.email,
    );
    await this.updateRefreshToken(user.id, refreshToken);

    return { accessToken, refreshToken, user };
  }

  /**
   * Sign in an existing user with the provided credentials.
   * @param {SignInInput} signInInput - The user's sign in input.
   * @returns {Promise<{accessToken: string, refreshToken: string, user: User}>} - The user's access token, refresh token, and user object.
   * @throws {ForbiddenException} If the user's email or password is incorrect.
   */
  async signin(signInInput: SignInInput) {
    const user = await this.prisma.user.findUnique({
      where: { email: signInInput.email },
      select: {
        id: true,
        email: true,
        hashedPassword: true,
        hashedRefreshToken: true,
        createdAt: true,
        updatedAt: true,
      },
    });
    if (!user) throw new ForbiddenException('Access Denied');

    const doesPasswordMatch = await argon.verify(
      user.hashedPassword,
      signInInput.password,
    );
    if (!doesPasswordMatch) throw new ForbiddenException('Access Denied');

    const { accessToken, refreshToken } = await this.createTokens(
      user.id,
      user.email,
    );
    await this.updateRefreshToken(user.id, refreshToken);

    return { accessToken, refreshToken, user };
  }

  /**
   * Sign in an existing user with the provided biometric key.
   * @param {BiometricInput} biometricSignin - The user's biometric sign in input.
   * @returns {Promise<{accessToken: string, refreshToken: string, user: User}>} - The user's access token, refresh token, and user object.
   * @throws {NotFoundException} If the user's biometric key is not found.
   */
  async biometricSignin(biometricSignin: BiometricInput) {
    const user = await this.prisma.user.findUnique({
      where: { biometricKey: biometricSignin.key },
      select: {
        id: true,
        email: true,
        hashedPassword: true,
        hashedRefreshToken: true,
        createdAt: true,
        updatedAt: true,
      },
    });

    if (!user) throw new NotFoundException('Account Not Found');

    const { accessToken, refreshToken } = await this.createTokens(
      user.id,
      user.email,
    );
    await this.updateRefreshToken(user.id, refreshToken);

    return { accessToken, refreshToken, user };
  }

  /**
   * Create JWT access and refresh tokens for the user.
   * @param {number} userId - The user's ID.
   * @param {string} email - The user's email.
   * @returns {Promise<{accessToken: string, refreshToken: string}>} - The user's access token and refresh token.
   */
  async createTokens(userId: number, email: string) {
    const accessToken = this.jwtService.sign(
      {
        userId,
        email,
      },
      {
        expiresIn: '10s',
        secret: this.configService.get('ACCESS_TOKEN_SECRET'),
      },
    );
    const refreshToken = this.jwtService.sign(
      {
        userId,
        email,
        accessToken,
      },
      {
        expiresIn: '7d',
        secret: this.configService.get('REFRESH_TOKEN_SECRET'),
      },
    );
    return { accessToken, refreshToken };
  }

  /**
   * Update the user's refresh token.
   * @param {number} userId - The user's ID.
   * @param {string} refreshToken - The user's refresh token.
   * @returns {Promise<void>}
   */
  async updateRefreshToken(userId: number, refreshToken: string) {
    const hashedRefreshToken = await argon.hash(refreshToken);
    await this.prisma.user.update({
      where: { id: userId },
      data: { hashedRefreshToken },
    });
  }
}
