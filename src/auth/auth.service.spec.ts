import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { PrismaService } from './../prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import * as argon from 'argon2';
import {
  BadRequestException,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';

describe('AuthService', () => {
  let service: AuthService;
  let prisma: PrismaService;
  let jwtService: JwtService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: PrismaService,
          useValue: {
            user: {
              findUnique: jest.fn(),
              create: jest.fn(),
              update: jest.fn(),
            },
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    prisma = module.get<PrismaService>(PrismaService);
    jwtService = module.get<JwtService>(JwtService);
    configService = module.get<ConfigService>(ConfigService);
  });

  describe('signup', () => {
    it('should throw BadRequestException if email exists', async () => {
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValueOnce({
        id: 1,
        email: 'test@example.com',
        hashedPassword: '',
        hashedRefreshToken: '',
        biometricKey: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      await expect(
        service.signup({
          email: 'test@example.com',
          password: 'password',
          biometricKey: 'key',
        }),
      ).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if biometric key exists', async () => {
      jest
        .spyOn(prisma.user, 'findUnique')
        .mockResolvedValueOnce(null)
        .mockResolvedValueOnce({
          id: 1,
          email: '',
          hashedPassword: '',
          hashedRefreshToken: '',
          biometricKey: 'key',
          createdAt: new Date(),
          updatedAt: new Date(),
        });

      await expect(
        service.signup({
          email: 'test2@example.com',
          password: 'password',
          biometricKey: 'key',
        }),
      ).rejects.toThrow(BadRequestException);
    });

    it('should create a new user and return tokens', async () => {
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue(null);
      jest.spyOn(prisma.user, 'create').mockResolvedValue({
        id: 1,
        email: 'test3@example.com',
        hashedPassword: '',
        hashedRefreshToken: '',
        biometricKey: 'key',
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      jest.spyOn(service, 'createTokens').mockResolvedValue({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
      });

      const result = await service.signup({
        email: 'test3@example.com',
        password: 'password',
        biometricKey: 'key',
      });

      expect(result).toEqual({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
        user: {
          id: 1,
          email: 'test3@example.com',
          hashedPassword: '',
          hashedRefreshToken: '',
          biometricKey: 'key',
          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),
        },
      });
    });
  });

  describe('signin', () => {
    it('should throw ForbiddenException if email does not exist', async () => {
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue(null);

      await expect(
        service.signin({ email: 'test@example.com', password: 'password' }),
      ).rejects.toThrow(ForbiddenException);
    });

    it('should throw ForbiddenException if password is incorrect', async () => {
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue({
        id: 1,
        email: 'test@example.com',
        hashedPassword: await argon.hash('wrongpassword'),
        hashedRefreshToken: '',
        biometricKey: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      });

      await expect(
        service.signin({ email: 'test@example.com', password: 'password' }),
      ).rejects.toThrow(ForbiddenException);
    });

    it('should return tokens if credentials are valid', async () => {
      const user = {
        id: 1,
        email: 'test@example.com',
        hashedPassword: await argon.hash('password'),
        hashedRefreshToken: '',
        biometricKey: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue(user);
      jest.spyOn(service, 'createTokens').mockResolvedValue({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
      });

      const result = await service.signin({
        email: 'test@example.com',
        password: 'password',
      });

      expect(result).toEqual({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
        user,
      });
    });
  });

  describe('biometricSignin', () => {
    it('should throw NotFoundException if biometric key is not found', async () => {
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue(null);

      await expect(service.biometricSignin({ key: 'key' })).rejects.toThrow(
        NotFoundException,
      );
    });

    it('should return tokens if biometric key is valid', async () => {
      const user = {
        id: 1,
        email: 'test@example.com',
        hashedPassword: '',
        hashedRefreshToken: '',
        biometricKey: 'key',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      jest.spyOn(prisma.user, 'findUnique').mockResolvedValue(user);
      jest.spyOn(service, 'createTokens').mockResolvedValue({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
      });

      const result = await service.biometricSignin({ key: 'key' });

      expect(result).toEqual({
        accessToken: 'accessToken',
        refreshToken: 'refreshToken',
        user,
      });
    });
  });

  describe('createTokens', () => {
    it('should create access and refresh tokens', async () => {
      jest.spyOn(configService, 'get').mockReturnValue('secret');
      jest.spyOn(jwtService, 'sign').mockImplementation(() => 'token');

      const result = await service.createTokens(1, 'test@example.com');

      expect(result).toEqual({ accessToken: 'token', refreshToken: 'token' });
    });
  });

  describe('updateRefreshToken', () => {
    it('should update the refresh token', async () => {
      jest.spyOn(argon, 'hash').mockResolvedValue('hashedRefreshToken');
      jest.spyOn(prisma.user, 'update').mockResolvedValue(null);

      await service.updateRefreshToken(1, 'refreshToken');

      expect(prisma.user.update).toHaveBeenCalledWith({
        where: { id: 1 },
        data: { hashedRefreshToken: 'hashedRefreshToken' },
      });
    });
  });
});
