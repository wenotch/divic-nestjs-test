import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator';
@InputType()
export class BiometricInput {
  @IsNotEmpty()
  @IsString()
  @Field()
  key: string;
}
